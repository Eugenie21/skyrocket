import './style/index.scss';
import $ from 'jquery';

import Form from './scripts/form';
import Map from './scripts/map';
import { Gallery } from './scripts/gallery';

function handleNavButtonClick() {
  const target = $(this).data('target');
  const $target = $(`#${target}`);

  if (!$target) {
    return;
  }

  $('#nav-menu-button').removeClass('is-open');
  $('#nav-menu').removeClass('show');

  $('html, body').animate({
    scrollTop: $target.offset().top
  }, 600);
}

$(document).ready(() => {

  // FORM
  $('.send-form').each(function() {
    new Form($(this));
  });

  // PHOTO GALLERY
  $('.Environment__Grid').each(function() {
    const gallery = new Gallery($(this));
    gallery.init();
  });

  // MAP
  $('.Map').each(function () {
    const map = new Map($(this));
    map.init();
  });

  // MODAL
  $('.Modal').each(function() {
    $(this).find('.Modal__CloseButton').click(() => {
      $(this).removeClass('is-active');
    });
  });

  $('.close-modal').each(function() {
    $(this).click(() => {
      $('.Modal').removeClass('is-active');
    });
  });

  // CALL BUTTON
  $('.call-button').click(() => {
    $('#call-modal').addClass('is-active');
  });

  // DOWN BUTTON
  $('#down-button').click(handleNavButtonClick);

  // SET ACTIVE NAV BUTTON
  const pathReg = /^\/([a-z]+)/;
  const pathMatch = pathReg.exec(location.pathname);
  $(`.Header__NavItem[data-navtarget="${pathMatch ? pathMatch[1] : ''}"]`).addClass('is-active');

  // MENU BUTTON
  $('#header-menu-btn').click(function() {
    $(this).toggleClass('is-active');
    $('#header-menu').toggleClass('is-active');
  });
});
