import $ from 'jquery';

export default class Map {

  constructor($container) {
    this.$container = $container;
    this.mapCenterCoords = {
      lg: { lat: 37.9519455, lng: 23.7419228 },
      md: { lat: 37.9519455, lng: 23.7259228 },
      sm: { lat: 37.9379455, lng: 23.7129228 }
    }
    this._updateMapCenter = this._updateMapCenter.bind(this);
    this._handleFullscreenChange = this._handleFullscreenChange.bind(this);
  }

  _renderFullScreenControl() {

    this.fullScreenControl = document.createElement('div');
    this.fullScreenControl.className = 'Map__FullscreenButton';

    google.maps.event.addDomListener(this.fullScreenControl, 'click', () => {
      const $originalControl = this.$container.find('.gm-control-active.gm-fullscreen-control');
      $originalControl.click();
    });

    this.fullScreenControl.index = 1;

    this.map.controls[google.maps.ControlPosition.TOP_CENTER].push(this.fullScreenControl);
  }

  _renderMarker() {
    new google.maps.Marker({
      icon: {
        url: 'images/svg/pin.svg', // url
        scaledSize: new google.maps.Size(50, 50), // scaled size
      },
      position: { lat: 37.9519455, lng: 23.7119228 },
      map: this.map
    })
  }

  _updateMapCenter() {

    /**
     $screen-xs: 360px;
     $screen-sm: 760px;
     $screen-md: 1024px;
     $screen-lg: 1280px;
     */

    const divElement = this.map.getDiv();
    const rect = divElement.getBoundingClientRect();
    const isLarge = rect.width > 1024;
    const isMedium = rect.width <= 1024 && rect.width > 760;

    if (isLarge) {
      this.map.panTo(this.mapCenterCoords.lg);
    } else if (isMedium) {
      this.map.panTo(this.mapCenterCoords.md);
    } else {
      this.map.panTo(this.mapCenterCoords.sm);
    }
  }

  _handleFullscreenChange(e) {
    $(this.fullScreenControl).toggleClass('is-open');
  }

  init() {
    const styles = [
      {
        "featureType": "all",
        "elementType": "labels.text.fill",
        "stylers": [
          {
            "saturation": 36
          },
          {
            "color": "#000000"
          },
          {
            "lightness": 40
          }
        ]
      },
      {
        "featureType": "all",
        "elementType": "labels.text.stroke",
        "stylers": [
          {
            "visibility": "on"
          },
          {
            "color": "#000000"
          },
          {
            "lightness": 16
          }
        ]
      },
      {
        "featureType": "all",
        "elementType": "labels.icon",
        "stylers": [
          {
            "visibility": "off"
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 20
          }
        ]
      },
      {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 17
          },
          {
            "weight": 1.2
          }
        ]
      },
      {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 20
          }
        ]
      },
      {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 21
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 17
          }
        ]
      },
      {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 29
          },
          {
            "weight": 0.2
          }
        ]
      },
      {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 18
          }
        ]
      },
      {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 16
          }
        ]
      },
      {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 19
          }
        ]
      },
      {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
          {
            "color": "#000000"
          },
          {
            "lightness": 17
          }
        ]
      }
    ];

    this.map = new google.maps.Map(
      this.$container.get(0),
      {
        zoom: 14,
        center: { lat: 37.9519455, lng: 23.7419228 },
        styles: styles,
        zoomControl: false,
        fullscreenControl: true,
        mapTypeControl: false,
        rotateControl: false,
        scaleControl: false,
        streetViewControl: false
      }
    );

    $('#fullscreen').click(function() {
      $('#container div.gm-style button[title="Toggle fullscreen view"]').trigger('click');
    });

    this._renderMarker();
    this._renderFullScreenControl();

    this._updateMapCenter();
    google.maps.event.addDomListener(this.map.getDiv(), 'fullscreenchange', this._handleFullscreenChange);
    // this.map.getDiv().addEventListener('fullscreenchange', this._handleFullscreenChange);
    window.addEventListener('resize', this._updateMapCenter);
  }
}
