import $ from 'jquery';

const RECAPTCHA_SITEKEY_V2 = '6LeG7_8UAAAAAKN_yNxcVhVOiirfKSmeeQk34pv2';
const RECAPTCHA_SITEKEY_V3 = '6Ley7_8UAAAAALI_POqIW_i7gRY8MUqnpHudkzL9';

export default class Form {

  constructor($form) {
    this.$form = $form;
    this.$captchaElement = this.$form.find('.Recaptcha');
    this.$submitButton = this.$form.find('button[type="submit"]');
    this.isV3 = this.$captchaElement.data('size') === 'invisible';

    this.handleFormSubmit = this.handleFormSubmit.bind(this);
    this.handleV2CaptchaSuccess = this.handleV2CaptchaSuccess.bind(this);
    this.sendData = this.sendData.bind(this);

    if (!this.isV3) {
      this.$submitButton.attr('disabled', true);
      this._renderCaptcha();
    }

    this._initListeners();
  }

  _initListeners() {
    this.$form.on('submit', this.handleFormSubmit);
  }

  _renderCaptcha() {
    grecaptcha.render(this.$captchaElement[0], {
      'sitekey': this.isV3 ? RECAPTCHA_SITEKEY_V3 : RECAPTCHA_SITEKEY_V2,
      ...(this.isV3 ? {} : {callback: this.handleV2CaptchaSuccess})
    });
  }

  handleV2CaptchaSuccess() {
    this.$submitButton.attr('disabled', false);
  }

  sendData() {

    const data = this.$form.serializeArray();

    if (data) {

      this.$form.find('input, textarea, button').attr('disabled', true);

      const sendOptions = {
        Host : "mail.betalife.tech",
        Username : "support@betalife.tech",
        Password : "1vaPAn1sa7",
        From : "support@betalife.tech",
        Subject : "This is the subject",
        Body : JSON.stringify(data)
      }

      Promise.all([
        Email.send({
          ...sendOptions,
          To: 'eugene2115@yandex.ru'
        }),
        // Email.send({
        //   ...sendOptions,
        //   To: 'm663005549@gmail.com'
        // }),
      ]).then(message => {
        $('#main-message').addClass('Message--show');
        this.$form.find('input, textarea').val('');
      }).finally(() => {
        this.$form.find('input, textarea, button').attr('disabled', false);
        $('.Modal').removeClass('is-active');
        const $messageModal = this.$form.find('.Message').closest('.Modal');
        $messageModal.addClass('is-active');
        // debugger;
      });
    }
  }

  handleFormSubmit(event) {
    event.preventDefault();

    if (this.isV3 && grecaptcha) {
      grecaptcha.execute(RECAPTCHA_SITEKEY_V3, { action: "social" })
        .then(this.sendData);
    } else {
      this.sendData();
    }
  }
}
