import $ from 'jquery';
import 'magnific-popup/dist/magnific-popup.css';
import 'magnific-popup/dist/jquery.magnific-popup.min';

export class Gallery {
  constructor($gallery) {
    this.$gallery = $gallery;
  }

  init() {
    this.$gallery.magnificPopup({
      delegate: '.Environment__Image__Wrap', // child items selector, by clicking on it popup will open
      type: 'image',
      gallery:{
        enabled: true
      }
      // other options
    });
  }
}
