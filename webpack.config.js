const path = require('path');
const HTMLWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  mode: process.env.NODE_ENV || 'development',
  entry: './src/index.js',
  output: {
    filename: 'js/main.js',
    path: path.resolve(__dirname, 'build')
  },
  devServer: {
    contentBase: path.join(__dirname, 'build'),
    compress: true,
    port: 8080
  },
  resolve: {
    extensions: ['.js', '.scss', '.pug']
  },
  module: {
    rules: [{
      test: /\.s?[ac]ss$/i,
      use: ExtractTextPlugin.extract({
        fallback: 'style-loader',
        use: [
          {
            loader: 'css-loader',
            options: {
              url: false
            }
          },
          'sass-loader'
        ]
      })
    }, {
      test: /\.(js)$/,
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader',
        options: {
          filename: 'js/main.[ext]'
        }
      }],
    }, /* {
      test: /\.(pug)$/,
      use: ['pug-loader']
    }, */ {
      test: /\.(png|jpe?g|gif|svg)$/i,
      loader: 'url-loader'
    }],
  },
  plugins: [
    // new HTMLWebpackPlugin({
    //   filename: 'index.html',
    //   template: './static/templates/index.pug'
    // }),
    // new HTMLWebpackPlugin({
    //   filename: 'space.html',
    //   template: './static/templates/space.pug'
    // }),
    // new HTMLWebpackPlugin({
    //   filename: 'services.html',
    //   template: './static/templates/services.pug'
    // }),
    // new HTMLWebpackPlugin({
    //   filename: 'service.html',
    //   template: './static/templates/service.pug'
    // }),
    // new HTMLWebpackPlugin({
    //   filename: 'pricing.html',
    //   template: './static/templates/pricing.pug'
    // }),
    // new HTMLWebpackPlugin({
    //   filename: 'contact.html',
    //   template: './static/templates/contact.pug'
    // }),
    new ExtractTextPlugin("css/style.css"),
    new CopyWebpackPlugin([
      {from: './static/images', to: './images'},
      {from: './static/fonts', to: './fonts'},
    ])
  ]
}
