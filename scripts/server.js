const express = require('express');
const path = require('path');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 8000;

app.use(cors({
  // origin: 'http://192.168.104.117:8000'
}));
app.use(express.static(path.join(__dirname, '../build')));
app.listen(port);

console.log(`Server is running on port 🚀${port}🚀`);
